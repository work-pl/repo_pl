#Date: 21/11/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Generate Html table

from CSVTable import Table
class HtmlTable:
    table = Table()
    headTable = 1

    def __init__(self):
        self.headTable = None

    def initialize(self):
        self.headTable = 1

    def htmlSintaxeCreate(self):
        try:
            t = HtmlTable()
            t.htmlFoot = "</table>"
            t.htmlHeader = ""
            self.headTable = 1
            for mycountry in self.table.countrys.keys():
                if self.headTable == 1:
                    t.htmlHeader = "<style>\n table, th, td \n {\ntext-align: center;\nborder: 1px solid black; \n border-collapse: collapse;} \n</style><tr>\n" \
                                "<table style=\"width:70%\"><tr>\n" \
                               "<th>"+mycountry+"</th><th>"+ self.table.capitals[mycountry]+"</th>" \
                               "<th>"+self.table.currencys[mycountry]+"</th><th>"+ self.table.primarylanguages[mycountry]+"</th>" \
                               "</tr>\n"
                    self.headTable += 1
                else:
                    t.htmlRows = "<tr><td>" + mycountry + "</td>\n\t<td>" + self.table.capitals[mycountry] + "</td>\n\t\t<td>" + self.table.currencys[mycountry] + "</td>\n\t\t\t<td>" + self.table.primarylanguages[mycountry] + "</td></tr>\n"
                    t.htmlHeader = t.htmlHeader + t.htmlRows
            t.htmlHeader = t.htmlHeader + t.htmlFoot
        except Exception as e:
            print(f"Erro: {e}")
        return t.htmlHeader

    def createHtmlTable(self):
        t = HtmlTable()
        hs = open("HTMLTable.html", 'w')
        if hs.write(t.htmlSintaxeCreate()):
            print("Html file created!")
        pass
