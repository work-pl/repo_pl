#Date: 21/11/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Choose the columm and a file to create

from TerminalSelectColumn import TerminalSelect
from repo_pl.generateLatex import LatexTable
from generateHtml import HtmlTable
latex = LatexTable()
html = HtmlTable()
table = TerminalSelect()
table.initialize()

#Permite que o utlizar escreva a coluna pretendida
for line in iter(lambda: input("\n>> "), ""):
    try:
        table.selectCollum(line)
        html.createHtmlTable()
        latex.createLatexTable()
        table.criateHtmlTablePersonalized()
        table.criateLatexTablePersonalized()
    except Exception as e:
        print(f"Erro: {e}")


