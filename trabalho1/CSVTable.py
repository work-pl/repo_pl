#Date: 21/11/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Recognize csv table
#Transformar numa classe

import re
import ply.lex as lex
from my_lib import readFile

class Table:

    tokens = ("COUNTRY","NL", "CAPITAL", "CURRENCY","PRIMARYLANGUAGE", "SYMBOL")

    states = (
                ("capital", "exclusive"),
                ("currency", "exclusive"),
                ("primarylanguage", "exclusive"),
        )

    ExReg = r"""([^",\n]+|"(?:[^"]|"")*")"""
    t_ANY_ignore = ","

    #reconhece new line
    def t_NL( t):
        r"\n"
        pass

    #Reconhece a coluna pais
    #Inicializa o estado capital
    @lex.TOKEN(ExReg)
    def t_COUNTRY(t):
        t.lexer.begin("capital")
        return t

    #Reconhece a coluna capital
    #Inciailiza o estado currency
    @lex.TOKEN(ExReg)
    def t_capital_CAPITAL(t):
        # r"""[^"]+"|[^,\r?\n]+"""
        t.lexer.begin("currency")
        return t

    #Reconhece a coluna currency
    #Inciailiza o estado primarylanguage
    @lex.TOKEN(ExReg)
    def t_currency_CURRENCY(t):
        t.lexer.begin("primarylanguage")
        return t

    #Reconhece a coluna primarylangage
    #Inciailiza o estado inicial que corresponde a coluna country
    @lex.TOKEN(ExReg)
    def t_primarylanguage_PRIMARYLANGUAGE( t):
        t.lexer.begin("INITIAL")
        return t

    @lex.TOKEN(ExReg)
    def primarylanguage_NL(t):
        r"[\n]"
        return t


    def t_error(t):
        print(f"Unexpected token: {t.value[:10]}")
        exit(1)
    def t_capital_error(self):
       self.t_error(self.t)

    def t_currency_error(self):
        self.t_error(self.t),

    def t_primarylanguage_error(self):
        self.t_error(self.t)

    #Mudar um simbolo para o simbolo novo
    def changeSymbol(self,newSymbol,oldSymbol, columnName):
        updateColumnName = newSymbol.join(re.split(oldSymbol, columnName))
        return updateColumnName

    lex.lexer = lex.lex()
    contents = readFile("list1.csv")
    lex.lexer.input(contents)

    lastcountry = None
    capitals = {}
    currencys = {}
    primarylanguages = {}
    countrys = {}

    def __int__(self):
        self.lexer = None

    def builder(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    for token in iter(lex.lexer.token, None):
            if token.type == "COUNTRY":
                lasttoken = token.value
                countrys[lasttoken] = lasttoken
            elif token.type == "CAPITAL":
                capitals[lasttoken] = token.value
            elif token.type == "CURRENCY":
                 currencys[lasttoken] = token.value
            elif token.type == "PRIMARYLANGUAGE":
                 primarylanguages[lasttoken] = token.value
