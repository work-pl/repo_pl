def readFile(file):
    try:
        with open(file, "r") as fh:
            contents = fh.read()
    except Exception as e:
        print(f"Erro: {e}")
    return contents
