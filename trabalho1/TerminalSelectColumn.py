#Date: 25/11/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Show a selected column

import ply.lex as lex

from CSVTable import Table

class TerminalSelect:
    table = Table()
    headTable = 1
    body = ""
    i = 1
    #Atributos

    t_ignore = " "
    tokens = ("COUNTRY", "CAPITAL", "CURRENCY", "PRIMARYLANGUAGE")
    t_COUNTRY = r"([C|c][ountry]+)|COUNTRY"
    t_CAPITAL = r"([C|c][apital]+)|CAPITAL"
    t_CURRENCY = r"([C|c][urrency]+)|CURRENCY"
    t_PRIMARYLANGUAGE = r"([P|p][rimary language]+)|PRIMARY LANGAUAGE"
    #html
    listTokens = []
    startHtlmLine = "<th>"
    endHtlmLine = "</th>\n"
    startHtlmcolumn = "<tr>"
    endHtlmcolumn = "</tr>\n"
    clearContents = ""
    Foot = "</table>"
    saveHeader = header = "<style>\n table, th, td \n {\ntext-align: center;\nborder: 1px solid black;\n"\
            "border-collapse: collapse;} \n</style><tr>\n" \
            "<table style=\"width:70%\">\n"
    tab1 = "\t"
    tab2 = "\t\t"
    tab3 = "\t\t\t"
    tab4 = "\t\t\t\t"
    #latex
    latexFoot = "\t\t\t\t\t\t\hline\n" \
                        "\t\t\t\t\t\end{tabular}\n" \
                        "\t\t\t\t\end{center}\n"\
                        "\t\t\t\end{table}\n"\
                        "\t\end{document}"


    saveLatexHeader = latexHeader = "\\documentclass[]{article}\n" \
              "\\usepackage[utf8]{inputenc}\n" \
              "\\begin{document}\n" \
              "\pdfpagewidth=279mm \n\pdfpageheight=700mm\n" \
              "\t\t\\begin{table}\n" \
              "\t\t\t\\begin{center}\n"

    firstElementInList= []
    lastElementInList = []
    middle = ""
    last = ""
    first = ""
#fim de atributos

    def t_error(self, t):
        print(f"Unexpected tokens: {t.value[:10]}")
        exit(1)

    #Construtor
    def __init__(self):
        self.lexer = None
        self.listTokens.clear()

    def initialize(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    #Insere a string no final de cada coluna Html
    def lastTokenInList(self, finalStringColumn, header):
        try:
            k = ""
            if self.listTokens[-1] == self.listTokens[self.i]:
                k = k + finalStringColumn
                header = header + k
                self.i = self.countElementList(self.listTokens, self.i)
        except Exception as e:
            print(f"Erro: {e}")
        return header


    #Contar Elementos os tokens na lista para poder criar colunas com os dados inseridos no terminal
    def countElementList(self, list, i):
        try:
            i = -1
            for element in list:
                i += 1
        except Exception as e:
            print(f"Erro: {e}")
        return i

    def columnCreatorLatex(self, list):
        try:
            tabulator = ""
            i = 0
            i = self.countElementList(list, i)
            if (i == 0):
                tabulator = ""
                tabulator = "\t\t\t\t\\begin{tabular}{|c|}\n"\
                            "\t\t\t\t\t\hline\n"
            elif (i == 1):
                tabulator = "\t\t\t\t\\begin{tabular}{|c|c|}\n" \
                            "\t\t\t\t\t\hline\n"

            elif (i == 2):
                tabulator = "\t\t\t\t\\begin{tabular}{|c|c|c|}\n" \
                            "\t\t\t\t\t\hline\n"
            elif (i == 3):
                tabulator = "\t\t\t\t\\begin{tabular}{|c|c|c|c|}\n" \
                            "\t\t\t\t\t\hline\n"
        except Exception as e:
            print(f"Erro: {e}")
        return tabulator

    #Definir Colunas no ficheiro Latex
    def tabulatorLatex(self, list):
        try:
            i = 0
            j = -1
            i = self.countElementList(list, i)
            while j <= i:
                if j == -1:
                    self.first = "------"
                elif j == 0:
                    self.first = "-----"
                elif j == 1:
                    self.last = "------"
                elif j == 2:
                    self.last = "------"
                j += 1
        except Exception as e:
            print(f"Erro: {e}")
        pass

    #Adicionar tokens na lista para poder criar tabela
    def addTokensInList(self, listTokens, lexerToken):
        try:
            for token in iter(lexerToken, None):
                listTokens.insert(len(listTokens), token.type)
        except Exception as e:
            print(f"Erro: {e}")
        pass

    #criate tabela personalizada em html
    def criateHtmlTablePersonalized(self):
        try:
            self.header = self.saveHeader
            for thiscountry in self.table.countrys.keys():
                if self.headTable == 1:
                    self.header = self.header + self.startHtlmcolumn
                    for token in self.listTokens:
                        if token == "COUNTRY":
                            self.header = self.header + self.tab1 + self.startHtlmLine + self.table.countrys[thiscountry] + self.endHtlmLine
                        if token == "CAPITAL":
                            self.header = self.header + self.tab2 + self.startHtlmLine + self.table.capitals[thiscountry] + self.endHtlmLine
                        if token == "CURRENCY":
                            self.header = self.header + self.tab3 + self.startHtlmLine + self.table.currencys[thiscountry] + self.endHtlmLine
                        if token == "PRIMARYLANGUAGE":
                            self.header = self.header + self.tab4 + self.startHtlmLine + self.table.primarylanguages[thiscountry] + self.endHtlmLine
                    self.headTable = self.headTable + 1
                    self.header = self.header + self.endHtlmcolumn
                    self.header = self.header + self.startHtlmcolumn
                else:
                    for tInList in self.listTokens:
                            self.body = self.clearContents
                            if tInList == "COUNTRY":
                                self.body = self.body + self.tab1 + self.startHtlmLine + self.table.countrys[thiscountry] + self.endHtlmLine
                            if tInList == "CAPITAL":
                                self.body = self.body + self.tab2 + self.startHtlmLine + self.table.capitals[thiscountry] + self.endHtlmLine
                            if tInList == "CURRENCY":
                                self.body = self.body + self.tab3 + self.startHtlmLine + self.table.currencys[thiscountry] + self.endHtlmLine
                            if tInList == "PRIMARYLANGUAGE":
                                self.body = self.body + self.tab4 + self.startHtlmLine + self.table.primarylanguages[thiscountry] + self.endHtlmLine
                            self.header = self.header + self.body
                            self.i = self.i - 1
                            self.header = self.lastTokenInList(self.endHtlmcolumn + self.startHtlmcolumn, self.header)

            self.header = self.header + self.Foot
            contents = open("HTMLChoosedColumn.html", 'w')
            contents.write(self.header)
            self.header = self.clearContents #Limpa caso o utilizador queira selecionar outras colunas
        except Exception as e:
            print(f"Erro: {e}")
        pass




    #Permite criate uma tabela em latex de acordo com a escolha do utilizador
    def criateLatexTablePersonalized(self):
        try:
            self.latexHeader = self.saveLatexHeader
            self.latexHeader = self.latexHeader + self.columnCreatorLatex(self.listTokens)
            self.tabulatorLatex(self.listTokens)
            self.headTable = 1
            for mycountry in self.table.countrys.keys():
                updateCountry = self.table.changeSymbol("\&", "&", mycountry)
                if self.headTable == 1:
                    for token in self.listTokens:
                        self.tabulatorLatex(self.listTokens)

                        if token == "COUNTRY":
                            self.latexHeader = self.latexHeader + "\t\t\t\t\t\t"+ self.first + updateCountry + self.last
                        if token == "CAPITAL":
                            self.latexHeader = self.latexHeader + self.first +"\t " + self.table.capitals[mycountry] + self.last
                        if token == "CURRENCY":
                            self.latexHeader = self.latexHeader +" \t"+ self.first +"\t " + self.table.currencys[mycountry] + self.last
                        if token == "PRIMARYLANGUAGE":
                            self.latexHeader = self.latexHeader + self.first + self.table.primarylanguages[mycountry] + self.last
                    self.latexHeader = self.latexHeader + "\\" + "\\" + "\n \t\t\t\t\t\t\hline\n"
                    self.headTable = self.headTable + 1
                else:
                    for tInList in self.listTokens:
                        self.tabulatorLatex(self.listTokens)
                        body = self.clearContents
                        if tInList == "COUNTRY":
                            body = body + "\t\t\t\t\t\t " + self.first + updateCountry + self.last
                        if tInList == "CAPITAL":
                            body = body + " \t" + self.first + "\t" + self.table.capitals[mycountry] + "\t" + self.last
                        if tInList == "CURRENCY":
                            body = body + " \t" + self.first + self.table.currencys[mycountry] + " \t"+ self.last+"\t"
                        if tInList == "PRIMARYLANGUAGE":
                            body = body + self.first + self.table.primarylanguages[mycountry] + self.last +"\\" + "\\" + "\n"
                        self.latexHeader = self.latexHeader + body
                        self.i = self.i - 1
                        self.latexHeader = self.lastTokenInList("\\\\\n", self.latexHeader)
            self.latexHeader = self.latexHeader + self.latexFoot
            contents = open("LATEXChoosedColumn.tex", 'w')
            contents.write(self.latexHeader)
        except Exception as e:
            print(f"Erro: {e}")
        pass

    #função que permite selecionar a coluna inserindo o titulo da coluna
    #Inserindo o titulo da coluna a função apresenta os dados da coluna inteira
    #Cria a tabela Html e latex personalizada
    #Adiciona Tokens na lista de tokens
    def selectCollum(self, columns):
        try:
            self.lexer.input(columns)
            self.listTokens.clear()
            self.addTokensInList(self.listTokens,self.lexer.token)
            self.i = self.countElementList(self.listTokens, self.i)
        except Exception as e:
            print(f"Erro: {e}")
        pass