import ply.lex as plex
from ply.lex import TOKEN
from my_lib import readFile
from pprint import PrettyPrinter  # pretty printer


class Table:
    tokens = ("COUNTRY", "CAPITAL", "CURRENCY", "PRIMARY")

    Expression = r"""(,|\r?\n|^)([^",\r\n]+)|"(?:[^"]|"")*"""

    @TOKEN(Expression)
    def t_STR(self, t):
        t.type = "COUNTRY"
        t.lexer.begin("capital")
        self.country = t.value
        pass

    @TOKEN(Expression)
    def t_country_STR(self, t):
        t.type = "COUNTRY"
        t.lexer.begin("currency")
        return t

    def t_primary_PRIMARY(self, t):
        r"""(,|\r?\n|^)([^",\r\n]+)|"(?:[^"]|"")*"""
        t.lexer.begin("primary")
        pass

    def t_capital_STR(self, t):
        r"[^,]+"
        t.lexer.begin("currency")
        t.type = "CAPITAL"
        pass

    # # lixo ao final da linha
    # def t_lixo_LIXO1(self, t):
    #     r".*\n"
    #     t.lexer.begin("INITIAL")
    #     return t

    # @TOKEN(Expression)
    # def t_currency_CURRENCY(self, t):
    #     (capitalC, _) = t.value
    #     if str(capitalC) == "Afghani":
    #         self.saveData[self.capital] = self.saveData.get(self.capital, 0) + 1
    #     t.lexer.begin("lixo")
    #     return t

    # nome da capital
    @TOKEN(Expression)
    def t_STR(self, t):
        "Documentacao"
        t.type = "CAPITAL"
        self.capital = t.value
        t.lexer.begin("country")
        return t

    def t_ANY_error(self, t):
        print(f"Unexpected token: {t.value[:10]}")
        exit(1)

    def t_LIXO2(self, t):
        r"\t.+\n"
        return t

    def __init__(self):
        self.lexer = None
        self.capital = None
        self.saveData = {}  # empty dictionary

    @staticmethod
    def builder(**kwargs):
        obj = Table()
        obj.lexer = plex.lex(module=obj, **kwargs)
        return obj

    def parse(self, filename):
        contents = readFile(filename)
        self.lexer.input(contents)
        for token in iter(self.lexer.token, None):
            pass
        return self.saveData


table = Table.builder()
capitals = table.parse("list1.csv")
pp = PrettyPrinter(indent=4, width=10)
pp.pprint(capitals)



