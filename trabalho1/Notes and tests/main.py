############## Chemistry.py
import ply.lex as plex
tokens = ("SQ", "NR")   #, "SPC"                                         )

# t_SQ = r"Cl?|O|H|Na|Zr"
# para todas as fórmulas químicas... ;-)
t_SQ = r"""(,|\r?\n|^)([^",\r\n]+)|"(?:[^"]|"")*"""

t_ignore = " "  # string com os chars a ignorar

# def t_SPC(t):
#     r"[ ]+"
#     pass

def t_NR(t):
    r"[0-9]+"
    t.value = int(t.value)
    return t


def t_error(t):
    print(f"Unexpected token: {t.value[:10]}")
    exit(1)


lexer = plex.lex()
for formula in iter(lambda: input(">> "), ""):
    lexer.input(formula)
    for t in iter(lexer.token, None):
        print(t)
