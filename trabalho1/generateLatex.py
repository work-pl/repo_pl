#Date: 21/11/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Generate latex table

import re
from CSVTable import Table

#implementar uma função que verifica se o latex gerado é valido
#implementar uma função que compara os dados originais com os dados reconhecidos
#retirar as aspas criar updates para as colunas de aspas e fazer re.split

class LatexTable:

    table = Table()
    headTable = 1
    countElements = 0
    updateCountry = None
    latexHeader = ""
    def __init__(self):
        self.headTable = None

    def initialize(self):
        self.headTable = 1

    #Constroi um ficheiro latex (tabela com 4 colunas)
    def latexSintaxeCreate(self):
        t = LatexTable()
        t.latexHeader = ""
        t.headTable = 1
        try:
            t.latexFoot = "\t\t\t\t\t\t\hline\n" \
                        "\t\t\t\t\t\end{tabular}\n" \
                        "\t\t\t\t\end{center}\n" \
                        "\t\t\t\end{table}\n" \
                        "\t\end{document}"
            for mycountry in self.table.countrys.keys():
                # updateCountry = "\&".join(re.split("&", mycountry))
                self.updateCountry = self.table.changeSymbol("\&", "&", mycountry)
                if t.headTable == 1:
                    self.latexHeader = "\\documentclass[]{article}\n" \
                                "\\usepackage[utf8]{inputenc}\n"\
                                "\\begin{document}\n"\
                                "\pdfpagewidth=279mm \n\pdfpageheight=700mm\n"\
                                "\t\t\\begin{table}\n"\
                                "\t\t\t\\begin{center}\n"\
                                "\t\t\t\t\\begin{tabular}{|c|c|c|c|}\n"\
                                "\t\t\t\t\t\hline\n" \
                               "\t\t\t\t\t\t" + self.updateCountry+" \t&\t "+ self.table.capitals[mycountry] +"\t &\t "+ self.table.currencys[mycountry] +" \t&\t "+ self.table.primarylanguages[mycountry] + "\\"+"\\"+ "\n"\
                                "\t\t\t\t\t\t\hline\n"

                    t.headTable = t.headTable + 1
                else:
                    self.table.primarylanguages[mycountry] = self.table.changeSymbol("","\"", self.table.primarylanguages[mycountry])

                    t.latexRows = "\t\t\t\t\t\t " + self.updateCountry + " \t&\t " + self.table.capitals[mycountry] + " \t&\t " + self.table.currencys[mycountry] + " \t&\t "+ self.table.primarylanguages[mycountry]  + "\\"+"\\"+"\n"
                    self.latexHeader = self.latexHeader + t.latexRows
                    t.countElements += 1
            self.latexHeader = self.latexHeader + t.latexFoot
        except Exception as e:
            print(f"Erro: {e}")
        return self.latexHeader


    def createLatexTable(self):
        try:
            hs = open("LATEXTable.tex", 'w')
            if hs.write(self.latexSintaxeCreate()):
                print("Latex file created!")
        except Exception as e:
            print(f"Erro: {e}")
        pass
