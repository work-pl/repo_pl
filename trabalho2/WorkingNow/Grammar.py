#Date: 30/12/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Reconhecedor lexico
#Lexer
#https://www.academia.edu/33541675/Aprendizado_de_Algoritmos_usando_o_Portugol_Studio

#S -> linha, ciclo ou comando para imprimir, funções
#e -> expressão numerica logica ou string expresão boleana
#e -> variavel é terminal
#b -> expressões bolenas atomo boleano
#ciclo -> ciclo for
#e-list -> uma sequencia de expresões separadas com virgulas
#numeros que são as expressões númericas
# Misturar a logica com a matemática

#

# func -> FUN var'(' var_list')' code ';' ENDFUN
# args -> € | var_list
# var_list -> € | var | var_list ',' var



import ply.yacc as pyacc
from pprint import PrettyPrinter

from Lexer import LogicLexer
from Eval import LogicEval


class LogicGrammar:

    precedence = (
        ("left", "ou",),
        ("left", "e"),
        ("left", "<", ">"),
        ("left", "=",),
        ("left", "+", "-"),
        ("left", "*", "/"),
        ("right", "uminus"),
    )

    def p_error(self, p):
        if p:
            raise Exception(f"Parse Error: Unexpected token '{p.type}'")
        else:
            raise Exception("Parse Error: Expecting token")

    def p_algoritmo1(self, p):
        """ algoritmo : codigo """
        p[0] = [p[1]]

    def p_algoritmo2(self, p):
        """ algoritmo : algoritmo ';' codigo  """
        p[0] = p[1] + [p[3]]  # concatena


    def p_inicio_fim(self, p):
        """ codigo : inicio list_comd fim """
        p[0] = p[2]


    def p_comando1(self, p):
        """ comando : expr
                    | ciclo
                    | condicao
                    | func"""
        p[0] = p[1]

    def p_atribuir(self, p):
        """ comando : ident atribuir expr  """
        p[0] = {"op": "atribuir", "args": [p[1], p[3]]}

    def p_declarar(self, p):
        """ comando : tipo ':' ident_list  """
        p[0] = {"op": p[1], "args": [ p[3]]}

    # def p_comando12(self, p):
    #     """ comando : tipo ':' list_ident  """
    #     p[0] = {"op": "declarar", "args": [p[1], p[3]]}

    def p_comando3(self, p):
        """ comando : escreva expr_list """
        p[0] = {"op": "escreva", "args": p[2]}

        # procedimento



    def p_ciclo_para(self, p):
        """ ciclo : para ident '[' expr ellipsis expr ']' list_comd ';' fimpara """
        p[0] = {
            "op": "for",
            "args": [p[2], p[4], p[6]],
            "data": [p[8]],
        }

    def p_para(self, p):
        """ ciclo : para '(' ident atribuir numero ';' cond ';' passo numero ')' list_comd ';' fimpara """
        p[0] = {
            "op": "para",
            "args": [p[3],p[5], p[7], p[10]],
            "data": [p[12]],
        }

    # Grmatica definida para enquanto
    def p_enquanto(self, p):
        """ ciclo : enquanto '(' expr '<' expr ')' list_comd ';' fimenquanto  """
        p[0] = {"op": "enquanto",
                "args":[p[4], p[3], p[5]],
                "data": [p[7],]
                }

    #
    # def p_while(self, p):
    #     """ ciclo : for ident '[' e ellipsis e ']' com_list ';' endfor """
    #     p[0] = {
    #         "op": "for",
    #         "args": [p[2], p[4], p[6]],
    #         "data": [p[8]],
    #     }

    # Escolha
        # de
        # Caso
        #
        # Outrocaso
        # Fimescolha(FIMESCOLHA)

    # def p_swicthCase(self, p):
    #     """ condicao : Escolha de caso  """
    #     pass

    # def p_comparacao(self, p):
    #    """ comparacao : e '<' e
    #                   | e '>' e """
    #    p[0] = {"op": p[2], "args": [p[1], p[3]]}

    # A funçaõ deve retornar um tipo
    #Por analisar
    def p_cria_funcao(self, p):
        """ func : tipo ':' funcao ident '(' args ')' list_comd ';' fimfuncao """
        p[0] = {
            "tipo" : p[1],
            "op": "funcao",
            "args": [],
            "data": [p[4], p[6], p[8]]
        }



    def p_funcao_se(self, p):
        """ condicao : se expr entao list_comd senao list_comd fimse """
        p[0] = {
            "op": "se",
            "args": [p[2]],
            "data": [p[4], p[6]]
        }


    # def p_declarar(self, p):
    #     """ declarar : tipo ':' ident_list """
    #     p[0] = {
    #             "atribuicao" : p[1],
    #             "data" : [p[3]]
    #
    #     }



    def p_expr_list(self, p):
        """ expr_list : expr
                   | expr_list ',' expr """
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    # Operações
    def p_exprs(self, p):
        """ exprs : expr_alg
                | expr_comp
                | expr_al
                | cond """

        p[0] = [p[1]]

    # números com menos
    def p_expr_alg1(self, p):
        """ expr_al : numero
              | '-' expr  %prec uminus  """
        p[0] = p[1] if len(p) == 2 else {"op": "-", "args": [0.0, p[2]]}

    #Expressões Algébricas
    def p_expr_alg(self, p):
        """  expr_alg : expr '+' expr
              | expr '-' expr
              | expr '*' expr
              | expr '/' expr """
        p[0] = dict(op=p[2], args=[p[1], p[3]])
    #Comparações > < =
    def p_expr_comp(self, p):
        """  expr_comp : expr '<' expr
              | expr '>' expr
              | expr '=' expr """
        p[0] = dict(op=p[2], args=[p[1], p[3]])

    def p_comp(self, p):
        """ cond : numero '<' numero
                | numero '>' numero
                | numero '=' numero
                """
        p[0] = dict(op=p[2], args=[p[1], p[3]])

    # analisar shift reduce em exprs
    # bollean
    # string
    # exprs -> comparação e algebra
    def p_expr3(self, p):
        """ expr : oplogico
                 | exprs
                 | string """
        p[0] = p[1]

    def p_expr(self, p):
        """ oplogico : logico
                      | expr ou expr
                      | expr e expr """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = dict(op=p[2], args=[p[1], p[3]])

    #

    def p_f1(self, p):
        """ logico : verdadeiro """
        p[0] = True

    def p_f2(self, p):
        """ logico : falso """
        p[0] = False

    def p_f3(self, p):
        """ logico : nao logico """
        p[0] = dict(op="nao", args=[p[2]])

    #

    def p_expr1(self, p):
        """ expr : ident """
        p[0] = {'ident': p[1]}

    def p_expr2(self, p):
        """ expr : '(' expr ')'  """
        p[0] = p[2]



    #para funcões e procedimentos
    def p_expr_ident(self, p):
        """ expr : ident '(' expr_list ')'
              | ident '(' ')' """
        p[0] = {"op": "call",
                "args": [],
                "data": [p[1], [] if p[3] == ")" else p[3]]}
                ###  [ factorial, [4.0, 5.0, 6.0]]


    def p_list_comd(self, p):
        """ list_comd : comando
                     | list_comd ';' comando """
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1]
            p[0].append(p[3])



    def p_ident_list(self, p):
        """ ident_list : ident
                     | ident_list ',' ident """
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1]
            p[0].append(p[3])



    def p_args(self, p):
        """ args :
                 | ident_list """
        p[0] = p[1] if len(p) == 2 else []

    def __init__(self):
        self.lexer = LogicLexer()
        self.tokens = self.lexer.tokens
        self.yacc = pyacc.yacc(module=self)

    def parse(self, expression):
        memory = self.yacc.parse(lexer=self.lexer.lex, input=expression)
        view = PrettyPrinter()
        view.pprint(memory)
        return LogicEval.eval(memory)




