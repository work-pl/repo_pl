#Date: 30/12/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Reconhecedor lexico
#Lexer

import ply.lex as lex


class LogicLexer:
    keywords = ("verdadeiro", "falso", "e", "ou", "para", "fimpara", "escreva", "leia", "inicio", "fim", "passo",
                "enquanto", "fimenquanto","nao", "funcao", "fimfuncao", "se", "fimse", "entao",
                "senao","tipo"
                )

    tokens = keywords + ("ident", "atribuir", "numero", "ellipsis", "string", )
    literals = "()+-/*;[],<>=:"
    t_ignore = " \t\n"
    def t_comment(self, t):
        r"""\#.*"""
        pass

    def t_tipo(self, t):
        r"""inteiro|real|logico"""
        return t

    def t_ellipsis(self, t):
        r"""\.{3}"""
        return t

    def t_string(self, t):
        r'"[^"]*"'
        t.value = t.value[1:-1]
        return t

    def t_numero(self, t):
        r"""[0-9]+(\.[0-9]+)?"""
        t.value = float(t.value)
        return t

    def t_atribuir(self, t):
        r"""<-"""
        return t

    def t_keywords(self, t):
        r"""[a-z]+"""
        t.type = t.value if t.value in self.keywords else "ident"
        return t

    def t_error(self, t):
        raise Exception(f"Unexpected token {t.value[:10]}")

    def __init__(self):
        self.lex = lex.lex(module=self)

    def token(self):
        return self.lex.token()







