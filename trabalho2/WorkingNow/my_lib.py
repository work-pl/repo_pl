#Date: 30/12/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Reconhecedor lexico
#Lexer

from Grammar import LogicGrammar

def run_batch(filename):
    with open(filename, "r") as f:
        content = f.read()
        lg = LogicGrammar()
        try:
            ans = lg.parse(content)
            if ans is not None:
                print(ans)
        except Exception as exception:
            print(exception)