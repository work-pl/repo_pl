#Date: 30/12/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Reconhecedor lexico
#Lexer

from pprint import PrettyPrinter as pp
from symbol_table import SymbolTable


class LogicEval:

    # Dispatch Table (Design Pattern)
    operators = {
        "ou": lambda args: args[0] or args[1],
        "e": lambda args: args[0] and args[1],
        "nao": lambda a: not a[0],
        "+": lambda args: args[0] + args[1],
        "-": lambda args: args[0] - args[1],
        "*": lambda args: args[0] * args[1],
        "/": lambda args: args[0] / args[1],
        "<": lambda args: args[0] < args[1],
        ">": lambda args: args[0] > args[1],
        "=": lambda args: args[0] == args[1],

        "atribuir": lambda a: LogicEval._assign(*a),
        "escreva": lambda a: print(*a),
        "for": lambda args: LogicEval._for(*args),
        "se": lambda args: LogicEval._if(*args),
        "funcao": lambda args: LogicEval._fun(args),
        "call": lambda args: LogicEval._call(args),
        "enquanto" : lambda args: LogicEval._enquanto(*args),
        "tipo" : lambda args: print(*args)
    }
    # Symbol Table (Tabela de Símbolos)
    symbols = SymbolTable()

    @staticmethod
    def _if(cond, then_code, else_code):
        return LogicEval.eval(then_code if cond else else_code)

    @staticmethod
    def check_float(x):
        assert type(x) is float, "operando nao é float"
        return x



    @staticmethod
    def _call(args):
        name, values = args
        name = f"{name}/{len(values)}"
        if name in LogicEval.symbols:
            code = LogicEval.symbols[name]["code"]
            var_list = LogicEval.symbols[name]["idents"]
            # 1. Definir as variaveis recebidas  [[DANGER]]
            for var_name, value in zip(var_list, values):
                LogicEval.symbols.re_set(var_name, LogicEval.eval(value))
                # LogicEval._assign(var_name, LogicEval.eval(value))
            # 2. Avaliar Codigo
            result = LogicEval.eval(code)
            # 3. Apagar as variaveis "locais"
            for var in var_list:
                del LogicEval.symbols[var]
            return result

        else:
            raise Exception(f"Function {name} not defined")

    @staticmethod
    def _fun(args):
        name, var, code = args
        name = f"{name}/{len(var)}"    # factorial/1
        LogicEval.symbols[name] = {"idents": var, "code": code}

    #enquanto
    @staticmethod
    def _enquanto(op, expr1, expr2, codigo):
        args = expr1, expr2
        if op in LogicEval.operators:
            func = LogicEval.operators[op]
        if func(args) == True:
            incr, comp = (1, lambda a, b: a != b) \
                if func(args) else (-1, lambda a, b: a == b)
            value = expr1
            while comp(value, expr2):
                value += incr
                LogicEval.eval(codigo)


    @staticmethod
    def _para(var, lower, higher, code):
        inc, comp = (1, lambda a, b: a <= b) \
            if lower < higher else (-1, lambda a, b: a >= b)
        value = lower
        LogicEval._assign(var, value)
        while comp(value, higher):
            LogicEval.eval(code)
            value += inc
            LogicEval._assign(var, value)


    @staticmethod
    def _assign(var, value):
        LogicEval.symbols[var] = value

    @staticmethod
    def eval(ast):
        if type(ast) in (float, bool, str):
            return ast
        if type(ast) is dict:
            return LogicEval._eval_dict(ast)
        if type(ast) is list:
            ans = None
            for c in ast:
                ans = LogicEval.eval(c)
            return ans
        raise Exception(f"Eval called with weird type: {type(ast)}")

    @staticmethod
    def _eval_dict(ast):
        if "op" in ast:
            op = ast["op"]
            # args = [LogicEval.eval(x) for x in ast["args"]]
            args = list(map(LogicEval.eval, ast["args"]))
            if "data" in ast:
                args += ast["data"]

            if op in LogicEval.operators:
                func = LogicEval.operators[op]
                return func(args)

            else:
                raise Exception(f"Unknown operator: {op}")
        elif "ident" in ast:
            if ast["ident"] in LogicEval.symbols:
                return LogicEval.symbols[ast["ident"]]
            raise Exception(f"Unknown variable {ast['ident']}")
        else:
            raise Exception("Weird dict on eval")

