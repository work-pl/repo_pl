def do_for():
    print("Execute for!")
    pass


class Eval:

    dispacth_table = {
        "int": do_for(),
        "+": lambda args: args[0] + args[1],
        "-": lambda args: args[0] - args[1],
        "*": lambda args: args[0] * args[1],
        "/": lambda  args: args[0] / args[1],
    }

    # Symbol Table (Tabela de Símbolos)
    symbols = {}

    @staticmethod
    def _assign(var, value):
        Eval.symbols[var] = value

    @staticmethod
    def eval(ast):
        if type(ast) in (float, bool, str):
            return ast
        if type(ast) is dict:
            return Eval._eval_dict(ast)
        if type(ast) is list:
            ans = None
            for c in ast:
                ans = Eval.eval(c)
            return ans
        raise Exception(f"Eval called with weird type: {type(ast)}")

    @staticmethod
    def _eval_dict(ast):
        if "op" in ast:
            op = ast["op"]
            # args = [LogicEval.eval(x) for x in ast["args"]]
            args = list(map(Eval.eval, ast["args"]))
            if "data" in ast:
                args += ast["data"]

            if op in Eval.operators:
                func = Eval.operators[op]
                return func(args)
            else:
                raise Exception(f"Unknown operator: {op}")
        elif "var" in ast:
            if ast["var"] in Eval.symbols:
                return Eval.symbols[ast["var"]]
            raise Exception(f"Unknown variable {ast['var']}")
        else:
            raise Exception("Weird dict on eval")
    def __init__(self, command, args):
        self.name = command
        self.args = args

    def __repr__(self):
         print(f"Command: {self.name}, {self.args}")
