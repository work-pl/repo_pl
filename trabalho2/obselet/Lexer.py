#Date: 30/12/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Reconhecedor lexico
#Lexer

import ply.lex as lex

class Lexer:

    keywords = ( "true", "false", "and", "not", "or", "const", "int","real", "char", "tipo" ,"inicio",  "fim", "se", "entao", "fim_se", "senao")
    tokens = keywords + ("ident", "numero", "function", "procedure", "comment", "STRING", "assign", "leitura", "escrita")

    literals = """:;,+-*().'""" #Reconhece todos carateres especiais
    t_ignore = " \n\t"

    def t_const(self, t):
        r""" \#define|const """

    #reconhece comentários
    def t_comment(self,t):
        r"""\#.*"""
        return t
    #Error

    #reconhece token inteiro
    def t_tipo(self, t):
        r"inteiro|real|carater|logico"
        return t


    #reconhece os números
    def t_numero(self, t):
        r"[0-9]+(\.[0-9]+)?"
        # t.value = float(t.value)
        return t

    #reconhece o simbolo para atribuir um valor a uma variavel
    def t_assign(self, t):
        r""" <- """
        t.value = t.value
        return t

    # reconhece as palavras reservadas
    # def t_identificador(self, t):
    #     r"""[a-zA-Z_][a-zA-Z0-9_]*"""
    #     return t

    #reconhece as palavras reservadas
    def t_keywords(self, t):
        r"""[a-zA-Z_][a-zA-Z0-9_]*"""
        t.type = t.value if t.value in self.keywords else "ident"
        return t


    def t_error(self, t):
        print(f"unknown token: {t.value}")
        exit(1)

    def t_leitura(self, t):
        r"""leia"""

    def t_escrita(self, t):
        r"""escreva"""

    #Construtor da classe
    def __init__(self):
        self.lexer = None

    #Inicilizar
    def Inicialize(self, contents, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)
        self.lexer.input(contents)