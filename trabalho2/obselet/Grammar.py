#Date: 04/12/2021
#Author: Joel Jonassi e José Matos Artur
#Title: Gramática
#construir a gramatica primeiro antes de implementar
#Parser


import sys
from repo_pl.trabalho2.obselet.Eval import Eval
from pprint import PrettyPrinter

from Lexer import Lexer
import ply.yacc as yacc

class Parser:

    precedence = (  # order matters
        ('left', '+', '-'),
        ('left', '*', '/'),
        ('left', 'or'),  # o OR tem menos prioridade
        ('left', 'and'),  # o AND é associativo à esquerda
        # primeiro faz a arvore dos que estão em cima e termina com os debaixo
        # visto que a  avaliçao da arvore começa de cima para baixo então os que estiveres por ultimo na lista são os primeiros a serem avaliados

    )
    # tokens = Lexer.tokens

    def p_algoritm1(self, p):
        """ algoritmo : inicio
                    | algoritmo codigo """
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            lst = p[1]
            lst.append(p[2])
            p[0] = lst

    # p[0] = {"var": [p[2], p[1]]}
    def p_codigo(self, p):
        """ codigo : listcomando ';' """
        p[0] = [p[1]]

    def p_comando1(self, p):
        """ listcomando : listcomando ';' comando ';'
                        | comando  ';' """
        if len(p) == 2:
            p[0] = p[1] + [p[3]]
        else:
            p[0] = [p[3]]

    def p_comando2(self, p):
        """ comando : inicio
                    | fim """
        p[0] = [p[1]]

    def p_declarar(self, p):
        """ comando : tipo ':' listident ';' 
                    | const ident expr ';' """  #FIXME averiguar const ou constante
        if len(p) == 4:
            p[0] = p[3]
        else:
            p[0] = p[3]


    def p_atribuir(self, p):
        """ comando : ident assign expr ';'
                    | ident assign ident ';' """
        p[0] = [p[3]]

    def p_list_ident(self, p):
        """ listident : ident
                    | listident ',' ident ';' """
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_numero(self, p):
        """ expr : numero ';'
                | expr numero ';' """
        if len(p) == 3:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    # def p_comando2(self, p):
    #     """ comando : listident assign expr """
    #     p[0] = Eval("inteiro", {"VARIAVEL": p[1], "EXPRESSAO" : p[3]})
    #
    #     p[0] = Eval("ident", p[3])



#####################################################################
    def p_expr(self, p):
        """ expr : expr '*' expr ';'
                | expr '/' expr ';'
                |  '(' expr ')'
                | '-' expr ';'
                | expr '+' expr ';'
                | expr '-' expr ';' """
        p[0] = dict(op=p[2], args=[p[1], p[3]])


################################################################
    # def p_comando5(self, p):
    #     """ listind : listind ',' identificador"""
    #     p[0] = Command("identificador", p[3])

    # def p_codigo1(self, p):
    #     """ codigo : variavel """
    #     p[0] = p[1]

    # def p_codigo1(self, p):
    #     """ codigo : inicio """
    #     p[0] = Command("inicio", p[1])

    # def p_codigo166(self, p):
    #     """ codigo : variavel assign number"""
    #     p[0] = Command('variavel', p[3])


    # def p_codigo19(self, p):
    #     """ codigo : int ':' variavel """
    #     p[0] = Command('Type', p[3])

    # def p_variavel(self, p):
    #     """ variavel : variavel "," identificador"""
    #     p[0] = p[2]

    # def p_variavel2(self, p):
    #     """ variavel : identificador"""
    #     p[0] = Command('variavel', p[1])
    #
    # def p_codigo229(self, p):
    #     """ codigo : expr number """
    #     p[0] = Command('expresao', p[1])



    # def p_codigo2(self,p):
    #     """ variavel : number """
    #     p[0] = p[1]

    # def p_codigo2(self,p):
    #     """ codigo : identificador """
    #     p[0] = p[1]

    def p_comment(self, p):
         """ codigo : comment """
         p[0] = p[1]

    # def __init__(self):
    #     self.parser = None
    #     self.lexer = None
    #
    def parse(self, input, **kwargs):
        self.lexer = Lexer()
        self.lexer.Inicialize(input, **kwargs)
        self.parser = yacc.yacc(module=self, **kwargs)
        ans = self.parser.parse(lexer=self.lexer.lexer)
        # for command in program:
        #     print(command)
        pp = PrettyPrinter()
        pp.pprint(ans)  # debug rulez!
        return Eval.eval(ans)

    def __init__(self):
        self.lexer = Lexer()
        self.tokens = self.lexer.tokens
        self.yacc = yacc.yacc(module=self)

    # def parse(self, expression):
    #     ans = self.yacc.parse(lexer=self.lexer.lex, input=expression)


    def p_error(self, t):
        print(f"Sytanx error", file=sys.stderr) #stderr manda o erro para o ficheiro
        print(t, file=sys.stderr)
        exit(1)

