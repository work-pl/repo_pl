import ply.yacc as yacc

def p_declaracao(t):
    "TIPO : IDENTIFICADORES;"
    pass

def p_comandosBasicos(t):
    "IDENTIFICADORES <- EXPRESSAO;"
    pass

def p_leitura(t):
    "LEIA(IDENTIFICADOR);"
    pass

def p_escreva(t):
    "ESCREVA();"
    pass

def p_condicional(t):
    "SE(CONDICAO) ENTAO \n COMANDOS \n FIIM_SE"
    pass

def p_error(t):
    print(f"Syntax error: {t.value}")
    exit()

