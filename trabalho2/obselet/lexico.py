#Trabalho realizado por Joel Jonassi e Jose Artur Matos
import ply.lex as lex

tokens = ("IDENTIFICADORES" , "TIPO" ,"OPERADORES")
t_ignore = "/**/"
literals = ":;,+-*/(){}[]!|=<>^.'%\"" #Reconhece todos carateres especiais


t_TIPO = r"inteiro|caracter|real|lógico"
t_IDENTIFICADOR = r"[a-zA-Z]+\d*" 
def t_OPERADORES ( self , t):
    self.logico=r"\+|-|\*|/"
    self.aritemetrico = r"e|ou|não"
    self.relacionais = r"=|<>|<=|>=|<|>"
    return t



def t_error(t):
    print(f"unknown token: {t.value}")
    exit(1)

 
def _init__(self):
    self.lexer = None

def Inicialize(self, contents, **kwargs):
    self.lexer = lex.lex(module=self, **kwargs)
    self.lexer.input(contents)